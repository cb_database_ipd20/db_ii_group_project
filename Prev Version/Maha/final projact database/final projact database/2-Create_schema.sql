/*
    Purpose: Create LighteningCo schema
    Script Date: Jan 30, 2020
    Developed By: Donghyeok Seo
 */

use LighteningCo
;
go

/* 1) create Productions schema */
create schema Productions authorization dbo
;
go

/* 2) create HumanResources schema */
create schema HumanResources authorization dbo
;
go

/* 3) create Sales schema */
create schema Sales authorization dbo
;
go

