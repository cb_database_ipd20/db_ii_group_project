
use LighteningCo
;
go
/*1-View: How many orders are done Last year*/--OKAY
alter view sales.lastYearOrders
as 
	select count(orderID) as 'orders done Last year'
	from sales.Orders
	where year(orderDate)=year(getDate()-1)
	;
	go
--test 
select*
from sales.lastYearOrders
;
go

/*2-QUERY:What percentage of the customer placed  at least 3 orders*/

select count(orderID) as 'number of orders',
	 count(customerID)*100.0/(select count(*) from sales.orders) as  'percentage'		
from sales.orders 
group by customerID
having count(orderID) > 10
;
go
/*3-VIEW:What was the greatest number of products ordered by any one individual*/
select top(1) count(PP.ModelNumber),
	SC.customerID,
	SO.OrderID
from sales.orders as SO
inner Join Sales.Customers as SC
on SO.CustomerID=SC.CustomerID
inner Join sales.OrderDetails as SOD
on SOD.OrderID=SO.OrderID
inner Join Productions.Products as PP
on SOD.ModelNumber=PP.ModelNumber
group by SC.customerID, SO.OrderID
;
go
/*4-Function:What percentage of the products was ordered at least once last year*/--SHOW  NOTHING WHEN EXECUTE
create function sales.percentageFN
(
 @@Number1 as int,
 @@Number2 as int
 )
 returns decimal
 as
 begin
 declare @percentage as decimal
 select @percentage =round((@@Number1*100.0/@@Number2),2)
 return @percentage
 end
 ;
 go
 select count(SO.orderID)as 'number of orders',	
	 --PP.ModelNumber as 'sales percentege'
	sales.percentageFN(count(PP.ModelNumber),(select count(*) from productions.products)) as 'sales percentage'
 from sales.orders as SO
 inner join sales.OrderDetails as SOD
 on so.OrderID=SOD.OrderID
 inner Join productions.products as PP
 on SOD.ModelNumber=PP.ModelNumber
 where year(OrderDate)=year(getdate())-1
 group by  PP.ModelNumber
 having count(SO.orderID) >1
 ;
 go
/*5-TRIGGER:What percentage of all orders eventually becomes overdue  (shipDate>RequiredDate).*/
create trigger sales.shipdateOrderDateTR
on sales.orders
after insert,update
as
begin
declare @shipDate as dateTime,
		@RequiredDate as dateTime
select  @shipDate =shipDate,
		@requiredDate =RequiredDate
		from inserted
		if( @shipDate>@requiredDate)
		begin
			raiserror('the order is overdue',
	10, --user severity
	1
	)
end
end
	;
	go
--test
insert into Sales.Orders (
/*6-PROCEDURE:What is the average quantity of orders knowing the ModelNumber*/--SHOW  NOTHING WHEN EXECUTE
create procedure sales.ordersAverageSP
(
	@ModelNumber as varchar(65)
	)
as
begin
select
	avg(Quantity) as 'average', 
	OrderID,
    ModelNumber
from sales.orderDetails
where ModelNumber like '@ModelNumber'--='Bianca-24W-BI18-40K-W20-SQ-AT'
group by OrderID,ModelNumber
end
;
go
--test
execute sales.ordersAverageSP  'Bianca-24W-BI18-40K-W20-SQ-AT'
;
go
select*
from sales.OrderDetails
;
go

drop procedure sales.ordersAverageSP
;
go
/*7-VIEW:What are the company�s peak days for orders*/--OKAY
alter view sales.peakDaysVIEW
as
select top(10) so.OrderDate as 'peak days for orders',
	count(so.orderID) as 'Number of Orders'
from sales.orders as so
group by so.OrderDate 
order by 'counted' desc
;
go
--test
select*
from sales.peakDaysView
;
                                  