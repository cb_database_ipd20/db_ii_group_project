/*1- Create a view Create a mailing list of CB clients (company name,contact name, Representative, email address)*/
create view sales.MailingListCustomerView
as 
	select c.companyName,c.ContactName,c.email
	from sales.Customers as c
;
go
 
/*2- Create a view  mailing list of CB employee (company name,contact name, Representative, email address)*/
create view humanResources.MailingListEmployeesView
as
	select FirstName,LastName,email
	from humanResources.Employees
;
go
/*3- Create a view to retrieve all the products by implementation */
create view  productions.productsImplementationView
as
select 
	PI.ImpleDescription as 'implementation description',
	PP.ProductCode as 'product code'
from Productions.Products as PP
	inner join Productions.Implementations as PI
	on PP.ImpleID=PI.ImpleID
;
go
select*
from productions.productsImplementationView
/*4- Create a view to retrieve all the products by series */
create view  productions.productsSeriesView
as
select 
	PS.SeriesName as 'series name',
	PP.ProductCode as 'product code'
from Productions.Products as PP
	inner join Productions.Series as PS
	on PP.SeriesID=PS.SeriesID
;
go
select* 
from productions.productsSeriesView
;
go
/*5-Query to retrieve the most popular product (most quantity of sales) for each series

select Top 1(PP.ProductCode) as 'top product',
	   PS.SeriesName as 'series name'
from Productions.products as PP
    inner join Productions.Series as PS
	on PP.SeriesID=PS.SeriesID 
where
(
	
;
go*/
/* 4. Query to retrieve all the products by series (like the excel file) */ /*CHECK THIS!!*/
select 
	PS.SeriesName as 'Series', 
	PP.ModelNumber as 'Model Number', 
	PP.ProductCode as 'Product Code', 
	CONCAT_WS(', ', PS.SeriesName, PC.ColorName, PK.KitName, PP.Power, CONCAT(PT.TempMin,' - ', PT.TempMax), PBA.Angle, PP.Note1) as 'Description', 
	PP.TierOnePrice as 'Tier 1', 
	PP.TierTwoPrice as 'Tier 2', 
	PP.MSRP as 'MSRP' 

from Productions.Products as PP
	inner join Productions.Series as PS
		on PP.SeriesID = PS.SeriesID
	inner join Productions.Colors as PC
		on PP.ColorID = PC.ColorID
	inner join Productions.Kits as PK
		on PP.KitID = PK.KitID
	inner join Productions.Temperature as PT
		on PP.TempID = PT.TempID
	inner join Productions.BeamAngle as PBA
		on PP.BeamAngleID = PBA.BeamAngleID
;	
go