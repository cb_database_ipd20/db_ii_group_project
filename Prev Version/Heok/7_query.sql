use LighteningCo;
go

-- 1-FUNCTION: How many orders are done  in a given year

-- 1-1 Function : when compare ShipDate and OrderDate for check constraint
IF OBJECT_ID('Sales.fn_ShipDate_OrderDate','FN') IS NOT NULL
	drop function Sales.fn_ShipDate_OrderDate;
go

create function Sales.fn_ShipDate_OrderDate
(
	@ShipDate as date,
	@OrderDate as date
) returns int
as
begin
	declare @result as int;
	set @result =
	case
		when (@ShipDate IS NULL) then 1
		when (@ShipDate >= @OrderDate) then 1
		else 0
	end
	return @result
end
;
go

-- 2-QUERY:What percentage of the customer placed  at least one order
-- 3-Function :What was the greatest number of products  ordered by any one individual
IF OBJECT_ID('Sales.fn_greatest_number_products','fn') is not null
	drop function Sales.fn_greatest_number_products;
go

create function Sales.fn_greatest_number_products(@CustomerID as nvarchar(5))
returns int
as
begin
declare @result as int;

set @result =
(
	select TOP 1
		SUM(SOD.quantity) as 'Total quantity'
	from Sales.Customers as SC
		join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
		join Sales.OrderDetails as SOD on SO.OrderID = SOD.OrderID
	where SC.CustomerID = @CustomerID
	group by SC.CustomerID,SOD.ModelNumber
	order by [Total quantity] desc
)

return @result;
end
;
go

IF OBJECT_ID('Sales.fn_greatest_number_modelNo','fn') is not null
	drop function Sales.fn_greatest_number_modelNo;
go
create function Sales.fn_greatest_number_modelNo(@CustomerID as nvarchar(5))
returns nvarchar(65)
as
begin
declare @modelNumber as nvarchar(65);


set @modelNumber =
(
	select TOP 1
		SOD.ModelNumber
	from Sales.Customers as SC
		join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
		join Sales.OrderDetails as SOD on SO.OrderID = SOD.OrderID
	where SC.CustomerID = @CustomerID
	group by SC.CustomerID,SOD.ModelNumber
	order by SUM(SOD.quantity) desc
)
return @modelNumber;
end
;
go

select CustomerID,
	Sales.fn_greatest_number_modelNo(CustomerID) as 'Model Number',
	Sales.fn_greatest_number_products(CustomerID) as 'Greatest Number of Products'
from Sales.Customers;



-- 4-PROCEDURE:What percentage of the products was ordered at least once last year

-- 5-TRIGGER:What percentage of all orders eventually becomes overdue  (shipDate>RequiredDate).



--- 5-TRIGGER: When data is deleted in the Company table,
-- save the information of user, time, company name, company ID and phone in the Customer_audit table
-- then move data of company in the Orders and OrderDetails tables to delOrders,delOrderDetails tables


/***** Table Customers_Audit ****/
create table Customers_Audit
(
    UserName nvarchar(20) not null default USER_NAME(),
    update_at datetime default GETDATE(),
    CustomerID nchar(5) not null,
    CompanyName nvarchar(40) not null,
    Phone nvarchar(25) not null,
    constraint pk_Customers_Audit primary key(UserName,CustomerID)
);
go

/***** Table delOrders ****/
create table delOrders
(
    OrderID int not null,  -- Primary Key
    CustomerID nchar(5) not null,   -- Foreign Key Customers (CustomerID)
    EmployeeID int not null,    -- Foerign key Employees (EmployeeID)
    OrderDate datetime null,
    RequiredDate datetime not null,
    ShipDate datetime null,
    ShipName nvarchar(40) null,
    ShipAddress nvarchar(60) null,
    ShipCity nvarchar(40) null,
    ShipPostalCode nvarchar(20) null,
    ShipRegion nvarchar(15) null,
    ShipCountry nvarchar(25) null,
    constraint pk_Orders primary key clustered (orderid asc)
);
go
/***** Table delOrderDetails ****/
create table delOrderDetails
(
    OrderID int not null,   -- Foreign Key Orders (OrderID)
    ModelNumber varchar(65) not null, -- Foreign Key Products (ModelNumber)
    UnitPrice money not null,
    Quantity smallint not null,
    constraint pk_OrderDetails primary key clustered (orderid asc,ModelNumber asc)
);
go


/* Define Triggers */
-- 1. Trigger for Sales.Customers DELETE
IF OBJECT_ID('Sales.trg_customer_audit','TR') IS NOT NULL
    DROP trigger Sales.trg_customer_audit;
go

create trigger Sales.trg_customer_audit
on Sales.Customers
after delete
as
begin
    SET NOCOUNT ON;

    insert into Customers_Audit (CustomerID,CompanyName,Phone)
        select d.CustomerID,d.CompanyName,d.Phone
        from deleted as d
    ;

end;
go

-- 2. Trigger for Sales.Orders on DELETE
IF OBJECT_ID('Sales.trg_orders_audit','TR') IS NOT NULL
    DROP trigger Sales.trg_orders_audit;
go

create trigger Sales.trg_orders_audit
on Sales.Orders
after delete
as
begin
    insert into delOrders
        select d.*
        from deleted as d
end
;
go


-- 3. Trigger for Sales.OrderDetails on DELETE
IF OBJECT_ID('Sales.trg_orderDetails_audit','TR') IS NOT NULL
    DROP trigger Sales.trg_orderDetails_audit;
go

create trigger Sales.trg_orderDetails_audit
on Sales.OrderDetails
after delete
as
begin
    SET NOCOUNT ON;

    insert into delOrderDetails
        select d.*
        from deleted as d
end
;
go


/* Test */
select count(*) as 'total record(Customers)'
from Sales.Customers;
go
select count(*) as 'total record(Orders)'
from Sales.Orders;
go
select count(*) as 'total record(OrderDetails)'
from Sales.OrderDetails;
go

delete from Sales.Customers
where CustomerID = 'ALFKI'
;

select *
from Customers_Audit;
select OrderID
from delOrders;
select *
from delOrderDetails;

select count(*) as 'total record(Customers)'
from Sales.Customers;
go
select count(*) as 'total record(Orders)'
from Sales.Orders;
go
select count(*) as 'total record(OrderDetails)'
from Sales.OrderDetails;
go

drop table Customers_Audit;
go
drop table delOrders;
go
drop table delOrderDetails;
go

-- 6-PROCEDURE:What is rank of the average quantity of orders of given Company name in this year

IF OBJECT_ID('Sales.average_quantity_per_customer', 'P') IS NOT NULL
    DROP PROCEDURE Sales.average_quantity_per_customer;
GO

create procedure Sales.average_quantity_per_customer
(
	--declare the prameters
	@customerID as nvarchar(5),
	@rank as int output
)
as
begin

--populate the output parameters
declare @quantity as int
set @quantity =
(
	select COUNT(OrderID)
	from Sales.Customers as SC
		join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
	where SC.CustomerID = @customerID -- @companyName
	    and YEAR(SO.OrderDate) = YEAR(GETDATE())
)

-- Find Rank
set @rank =
(
    select COUNT(*)
    from
    (
        select distinct(QO.[Quantity of Orders]) as Quantity
        from(
            select SC.CompanyName, COUNT(SO.OrderID) as 'Quantity of Orders'
            from Sales.Customers as SC
                 join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
            where --SC.CustomerID = 'ALFKI' and
                 YEAR(SO.OrderDate) = YEAR(GETDATE())
             group by SC.CompanyName
        ) as QO
    ) as R
    where R.Quantity > @quantity
)
set @rank = @rank + 1
end
;
go


select ROW_NUMBER() OVER (ORDER BY QO.[Quantity of Orders] desc) as Rank,
	    QO.[Quantity of Orders] as Quantity
from(
	select SC.CompanyName, COUNT(SO.OrderID) as 'Quantity of Orders'
	from Sales.Customers as SC
		join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
	where YEAR(SO.OrderDate) = YEAR(GETDATE())
	group by SC.CompanyName
) as QO
group by (QO.[Quantity of Orders])
order by QO.[Quantity of Orders] desc;
go

select SC.CompanyName, COUNT(SO.OrderID) as Quantity
from Sales.Customers as SC
    join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
where SC.CustomerID = 'ALFKI'
    and YEAR(SO.OrderDate) = YEAR(GETDATE())
group by SC.CompanyName
;
go

declare @rank as int
execute Sales.average_quantity_per_customer 'ALFKI', @rank output
select @rank as 'ALFKI_Rank';
go


-- 7-VIEW:What are the company’s peak days for orders

IF OBJECT_ID('Sales.vw_peak_days_orders','V') IS NOT NULL
    DROP VIEW Sales.vw_peak_days_orders;
go

create view Sales.vw_peak_days_orders
as
select SC.CustomerID as 'CustomerID', SO.OrderDate, Count(SO.OrderID) as 'Total Order'
from Sales.Customers as SC
	join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
group by SC.CustomerID, SO.OrderDate
;
go

select *
from Sales.vw_peak_days_orders
order by CustomerID
;
go

