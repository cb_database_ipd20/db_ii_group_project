/*
    Purpose: Apply data integrities
    Script Date: Jan 30, 2020
    Developed By: Donghyeok Seo
 */


use LighteningCo
;
go


/* Import Products data */
BULK INSERT Productions.Products
    FROM 'C:\ProgramData\dataSample\Products.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ProductsErrorRows.csv',
    TABLOCK
);

/* Import Series data */
BULK INSERT Productions.Series
    FROM 'C:\ProgramData\dataSample\Series.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\SeriesErrorRows.csv',
    TABLOCK
);
go


/* Import BeamAngle data */
BULK INSERT Productions.BeamAngle
    FROM 'C:\ProgramData\dataSample\BeamAngle.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\BeamAngleErrorRows.csv',
    TABLOCK
);
go


/* Import Implementations data */
BULK INSERT Productions.Implementations
    FROM 'C:\ProgramData\dataSample\Implementations.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ImplementationsErrorRows.csv',
    TABLOCK
);
go


/* Import Temperature data */
BULK INSERT Productions.Temperature
    FROM 'C:\ProgramData\dataSample\Temperature.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\TemperatureErrorRows.csv',
    TABLOCK
);
go


/* Import Kits data */
BULK INSERT Productions.Kits
    FROM 'C:\ProgramData\dataSample\Kits.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\KitsErrorRows.csv',
    TABLOCK
);


/* Import Colors data */
BULK INSERT Productions.Colors
    FROM 'C:\ProgramData\dataSample\Colors.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ColorsErrorRows.csv',
    TABLOCK
);


/* Import Images data */
BULK INSERT Productions.Images
    FROM 'C:\ProgramData\dataSample\Images.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ImagesErrorRows.csv',
    TABLOCK
);


/* Import ProductImage data */
BULK INSERT Productions.ProductImage
    FROM 'C:\ProgramData\dataSample\ProductImage.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
  --  ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\ProductImageErrorRows.csv',
    TABLOCK
);


/* Import Orders data */
BULK INSERT Sales.Orders
    FROM 'C:\ProgramData\dataSample\Orders.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\OrdersErrorRows.csv',
    TABLOCK
);


/* Import OrderDetails data */
BULK INSERT Sales.OrderDetails
    FROM 'C:\ProgramData\dataSample\OrderDetails.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\OrderDetailsErrorRows.csv',
    TABLOCK
);

/* Import Customers data */
BULK INSERT Sales.Customers
    FROM 'C:\ProgramData\dataSample\Customers.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
 --   ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\CustomersErrorRows.csv',
    TABLOCK
);


/* Import Employees data */
BULK INSERT HumanResources.Employees
    FROM 'C:\ProgramData\dataSample\Employees.csv'
    WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
--    ERRORFILE = 'C:\ProgramData\dataSample\ErrorRows\EmployeesErrorRows.csv',
    TABLOCK
);

