1- Make Montreal DEFAULT city when country is canada
2-create new COLUMN in customer table for email
3-create CHECK constraint (shipDate>orderDate)
4-create CHECK constraint(tier1<tier2<msrp
5-Add email address to theEmployee 
6-Add availability to the Products table (in stock, out of stock, discontinued)


Task 3.
======
* Create a mailing list of CB clients (First, Last Name, Representative, email address)
* Create a mailing list of CB employees (First, Last Name, email address)

* Query to retrieve all the products by implementation (like their website)
* Query to retrieve all the products by series (like the excel file)
* Query to retrieve the most popular product (most quantity of sales) for each family
* Query to retrieve the most popular product (most quantity of sales) for CB each year
* Query to retrieve the employee with most sales (each year?) 

* Query to retrieve all the products of a particular color (sort by family, product code, implementation)
* Query to retrieve all the products of a specific beam angle (sort by family, product code, implementation)

* View list of CB clients (re-sellers) (First, Last Name, Representative, Address, City, Postal Code, Country)
* View to retrieve all the product information by product code (or model number)
* View 	to retrieve the list of distributors by city (display company name, city, country, contact info)

1-create mailing list for the employees(query)
2-Write and execute a query on the products, implementatios, order details that returns the oreders price subtotal , product's seriesid, impldescript,when the price is higher than2000$(query)
3-VIEW returns the full name and full address for the customers
4-VIEW returns the (customers full names,number of customers, number of orders) for specific country
5-VIEW to list informaion about the orders that have shipDate inthe first quarter of the year


Generating Usage Reports: 
=======================
1-FUNCTION: How many orders are done  in a given year
2-QUERY:What percentage of the customer placed  at least one order
3-VIEW:What was the greatest number of products  ordered by any one individual
4-PROCEDURE:What percentage of the products was ordered at least once last year
5-TRIGGER:What percentage of all orders eventually becomes overdue  (shipDate>RequiredDate).
6-PROCEDURE:What is the average quantity of orders
7-VIEW:What are the company�s peak days for orders

